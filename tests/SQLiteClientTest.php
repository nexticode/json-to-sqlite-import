<?php declare (strict_types = 1);

require __DIR__ . "/../public/sqlite/client.php";

use PHPUnit\Framework\TestCase;

final class SQLiteClientTest extends TestCase
{

    protected $client;

    protected function setUp(): void
    {
        $this->client = new SqliteClient();
    }

    public function testFindTableCols(): void
    {
        $model = new stdClass;
        $model->key1 = 'test';
        $model->key2 = 'test';

        $this->assertIsArray(
            $this->invokeMethod($this->client, 'findTableCols', array([$model, $model]))
        );
        /** Test with passing array */
        $this->assertEquals(
            $this->invokeMethod($this->client, 'findTableCols', array([$model, $model])),
            ['key1', 'key2']
        );
        /** Test with passing object */
        $this->assertEquals(
            $this->invokeMethod($this->client, 'findTableCols', array($model)),
            ['key1', 'key2']
        );
    }

    public function testFindTableName(): void
    {
        $this->assertEquals(
            $this->invokeMethod($this->client, 'findTableName', array('posts_en.json')),
            'posts'
        );
    }

    public function testPrepareForQuery(): void
    {
        $this->assertEquals(
            $this->invokeMethod($this->client, 'preapreForQuery', array('payment institution')),
            'payment+institution*'
        );
    }

    public function testFileExist(): void
    {

        $this->assertTrue(
            $this->invokeMethod($this->client, 'fileExists', array('posts_en.json'))
        );

        $this->assertFalse(
            $this->invokeMethod($this->client, 'fileExists', array('wrongFile.json'))
        );

    }
    /**
     * Allows to fully test a calls including private methods using reflection
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $parameters);
    }

}
