<?php

include_once './sqlite/client.php';

if (!isset($_GET['search'])) {
    echo "Param search is required.";
    die();
}

if (!isset($_GET['type'])) {
    echo "Param type is required.";
    die();
}

$client = new SqliteClient();
$client->init('pagofx_db');

$query = strip_tags($_GET['search']);

$type = strip_tags($_GET['type']);

$posts = $client->search($type, $query);

echo $posts;
