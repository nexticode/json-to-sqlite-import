<?php
/**
 * this file can only run from CLI
 *
 *  To run the script pass the path to the files to import like
 *
 * > php cli.php ./data
 *
 */
if (PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) {
    die("cli only :)");
}

if (!$argv[1]) {
    die("please add the directory path to the files you are looking to import");
}

if (!file_exists($argv[1]) || !is_dir($argv[1])) {
    die("Provided path does not exist or is not a directory");
}

include_once './sqlite/client.php';

import($argv[1]);

function import($path)
{
    $client = new SqliteClient();
    $client->init('pagofx_db');

    $allFiles = scandir($path);
    $files = array_diff($allFiles, array('.', '..'));

    foreach ($files as $file) {
        if (!is_dir($file)) {
            $client->importJson($file, $searchable = true);
        }
    }

}
