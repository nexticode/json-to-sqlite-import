<?php

class SqliteClient
{
    private $db;
    public $path = __DIR__ . '/../data/';

    private $searchable = [
        'title',
        'excerpt',
        'content',
        'answer',
    ];

    public function __construct($filePath = false)
    {
        if ($path) {
            $this->path = $filePath;
        }
    }

    public function init($name = 'default_db'): void
    {
        $this->db = new PDO("sqlite:{$name}.sqlite3");
    }

    private function insert($table, $data): int
    {
        $sCols = "";
        $sValues = "";

        foreach ($data as $key => $value) {
            $sCols .= "{$key},";

            if (is_array($value)) {
                $value = json_encode($value);
            }

            $sValues .= "{$this->db->quote(strip_tags($value))},";
        }

        $sCols = rtrim($sCols, ",");
        $sValues = rtrim($sValues, ",");

        if (empty($sCols) || empty($sValues)) {
            return false;
        }

        $SQL = "INSERT INTO {$table}({$sCols}) VALUES({$sValues})";

        $this->db->exec($SQL);

        return $this->db->lastInsertId();
    }

    private function insertVirtual($table, $cols, $id, $data): int
    {
        $sValues = "";

        $supported = array_intersect($this->searchable, $cols);
        $cols = implode(",", $supported);

        foreach ($supported as $value) {
            if ($data->{$value}) {
                $sValues .= "{$this->db->quote($data->{$value})},";
            }
        }

        $sValues .= "{$id}";

        if (empty($sValues)) {
            return false;
        }

        $SQL = "INSERT INTO virtual_{$table}({$cols}, {$table}_id) VALUES({$sValues})";

        $this->db->exec($SQL);

        return $this->db->lastInsertId();
    }

    private function createTable($name, $cols): void
    {
        $tableCols = array_reduce(
            array_map(
                function ($col) {return "{$col} TEXT NOT NULL";}
                , $cols
            ),
            function ($string, $item) {return $string .= ", {$item}";}
        );

        $SQL = "CREATE TABLE IF NOT EXISTS {$name} (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL
            {$tableCols}
          )";

        $this->db->exec($SQL);
    }

    public function search($table, $query)
    {
        $query = $this->preapreForQuery($query);

        $SQL = "SELECT {$table}.* FROM {$table}
                JOIN virtual_{$table} ON {$table}.id = virtual_{$table}.{$table}_id
                WHERE virtual_{$table} MATCH '{$query}'
                ORDER BY rank;";

        $results = $this->db->query($SQL);

        if ($results) {
            $results = $results->fetchAll(PDO::FETCH_CLASS);
        }

        return json_encode($results);
    }

    public function importJson($file, $isSearchable = true): void
    {
        if ($this->fileExists($file)) {
            $fileContent = json_decode(file_get_contents($this->path . $file));
        }

        $tName = $this->findTableName($file);

        if ($tCols = $this->findTableCols($fileContent)) {

            $this->createTable($tName, $tCols);

            if ($isSearchable) {
                $this->createVirtualTable($tName, $tCols);
            }

            array_map(
                function ($item) use ($tName, $tCols, $isSearchable) {

                    $id = $this->insert($tName, $item);

                    if ($isSearchable) {
                        $this->insertVirtual($tName, $tCols, $id, $item);
                    }
                },
                $fileContent
            );
        }

    }

    private function preapreForQuery($query): String
    {
        $query = str_replace(" ", "+", strip_tags($query));
        $query = "{$query}*";

        return $query;
    }

    private function createVirtualTable($table, $cols): void
    {
        $supported = array_intersect($this->searchable, $cols);
        $supCols = implode(",", $supported);
        $SQL = "CREATE VIRTUAL TABLE virtual_{$table} USING FTS5({$supCols}, {$table}_id);";
        $this->db->exec($SQL);
    }

    private function findTableCols($data)
    {
        if (!$data) {
            return false;
        }

        if (is_array($data)) {
            $model = $data[0];
        }

        if (is_object($data)) {
            $model = $data;
        }

        return array_keys((array) $model);
    }

    private function findTableName($file): String
    {
        $name = explode('_', $file);
        $name = $name[0];
        return $name;
    }

    private function fileExists($file): bool
    {
        if (file_exists($this->path . $file)) {
            return true;
        }

        return false;
    }

}
